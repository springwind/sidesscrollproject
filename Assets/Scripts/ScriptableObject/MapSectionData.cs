using UnityEngine;

[System.Serializable]
public class MapSectionDataElem
{
    public int mapSectionID;
    public string mapSectionPath;
}