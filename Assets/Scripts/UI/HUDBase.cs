﻿using UnityEngine;
public class HUDBase : MonoBehaviour
{
    /// <summary>
    /// 표시될 월드 위치를 나타냅니다.
    /// </summary>
    private Vector3 _WorldPosition;


    private RectTransform rectTransform => transform as RectTransform;

    /// <summary>
    /// HUD 객체가 초기화될 때 호출됩니다.
    /// </summary>
    public virtual void OnHUDInitialized()
    {
        rectTransform.anchorMin = rectTransform.anchorMax = Vector2.zero;

        rectTransform.pivot = new Vector2(0.5f, 0.0f);
    }

    public virtual void SetWorldPosition(Vector3 worldPosition)
    {
        _WorldPosition = worldPosition;

        GameSceneInstance sceneInstance =
            SceneManager.instance.GetSceneInstance<GameSceneInstance>();
        Camera cameraComponent = sceneInstance.m_UseFollowCamera.cameraComponent;

        // 화면 위치로 변환합니다.
        Vector3 screenPosition = cameraComponent.WorldToViewportPoint(_WorldPosition);

        screenPosition *= Constants.SCREENSIZE;

        rectTransform.anchoredPosition = screenPosition;


    }
}
