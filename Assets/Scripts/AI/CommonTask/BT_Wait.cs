﻿using UnityEngine;
using System.Collections;

public class BT_Wait : RunnableBehavior
{
    private WaitForSeconds _WaitSeconds;

    public BT_Wait(float waitSeconds)
    {
        _WaitSeconds = new WaitForSeconds(waitSeconds);

    }
    public override IEnumerator RunBehavior()
    {
        yield return _WaitSeconds;
        isSucceeded = true;
    }
}

