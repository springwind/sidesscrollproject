﻿using System.Collections;
using UnityEngine;
public class BT_Enemy01Attack : RunnableBehavior
{
    public override IEnumerator RunBehavior()
    {
        Enemy01 enemyCharacter = behaviorController.GetComponent<Enemy01>();

        // 공격한 시간을 기록합니다.
        enemyCharacter.behaviorController.SetKey(
            BehaviorController_Enemy01.KEYNAME_LASTATTACKTIME, Time.time);

        //공격 요청을 받았으므로 공격 요청을 취소시킵니다.
        enemyCharacter.behaviorController.SetKey(BehaviorController_Enemy.KEYNAME_ATTACKREQUESTED, false);

        // 이동을 잠시 멈춥니다.
        enemyCharacter.behaviorController.SetKey(BehaviorController_Enemy01.KEYNAME_BLOCKMOVEMENT, true);

        // 공격 애니메이션 재생
        enemyCharacter.attackComponemt.Attack();

        isSucceeded = true;
        yield return null;
    }
}

