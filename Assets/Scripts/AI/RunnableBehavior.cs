using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Behavior Controller 를 통해 실행될 수 있는 객체의 최상위 기반 형식
/// </summary>
public abstract class RunnableBehavior
{
	/// <summary>
	/// 이 객체에서 사용되는 서비스 객체들을 나타냅니다.
	/// </summary>
	private List<System.Func<BehaviorService>> _Services = new();

	/// <summary>
	/// 서비스 루틴을 나타냅니다.
	/// </summary>
	private Coroutine _ServiceRoutine;

	/// <summary>
	/// 생성된 서비스 객체들을 나타냅니다.
	/// </summary>
	public List<BehaviorService> behaviorServices { get; private set; }

	/// <summary>
	/// 이 행동을 실행하는 BehavirController 객체입니다.
	/// </summary>
	public BehaviorController behaviorController { get; private set; }

	/// <summary>
	/// 실행시킨 행동의 성공 여부를 나타낸븐 프로퍼티입니다.
	/// </summary>
	public bool isSucceeded { get; protected set; }

	/// <summary>
	/// 행동 객체가 초기화될 때 호출됩니다.
	/// 생성자 호출 이후에 호출됩니다.
	/// </summary>
	/// <param name="behaviorController">BehaviorController 객체가 전달됩니다.</param>
	public virtual void OnRunnableInitialized(BehaviorController behaviorController)
	{
		this.behaviorController = behaviorController;

		// 실행시킬 서비스가 존재한다면
		if(_Services.Count != 0)
		{
			behaviorServices = new();
			foreach(System.Func<BehaviorService> getService in _Services)
			{
				// 서비스 객체 생성
				BehaviorService runService = getService.Invoke();

				// 서비스 객체 초기화
				runService.OnServiceStarted(behaviorController);

				// 실행시킬 서비스 객체 등록
				behaviorServices.Add(runService);
			}
			// 서비스 실행
			_ServiceRoutine = behaviorController.StartCoroutine(RunService());

		}


    }

	/// <summary>
	/// 행동을 실행시킵니다.
	/// 구현 시 내부에서 행동 실행 성공 여부를 isSucceeded 에 설정해야 합니다.
	/// </summary>
	/// <returns></returns>
	public abstract IEnumerator RunBehavior();

	/// <summary>
	/// 서비스 틱을 위한 루틴입니다.
	/// </summary>
	public virtual IEnumerator RunService()
	{
		while (true)
		{
			foreach  (BehaviorService service in behaviorServices)
			{
				service.ServiceTick();
			}
			yield return null;
		}
	}

	/// <summary>
	/// 행동이 끝나는 경우 호출됩니다.
	/// </summary>
	public virtual void OnBehaviorFinished()
	{
		// 서비스를 실행하고 있다면
		if(_ServiceRoutine != null)
		{
			// 서비스 루틴 종료
			behaviorController.StopCoroutine(_ServiceRoutine);
			_ServiceRoutine = null;

            // 서비스 종료
            foreach  (BehaviorService service in behaviorServices)
			{
				service.OnServiceFinished();
			}

			// 서비스 객체들 비우기
			behaviorServices.Clear();
		}
	}

	/// <summary>
	/// 사용할 서비스를 추가합니다.
	/// </summary>
	/// <typeparam name="TBehaviorService">서비스 형식을 전달합니다.</typeparam>
	public void AddService<TBehaviorService>()
		where TBehaviorService : BehaviorService, new()
		=>_Services.Add(() => new TBehaviorService());
	

	public void AddService(System.Func<BehaviorService>runService)
		=> _Services.Add(runService);


	/// <summary>
	/// 행동 객체를 등록하기 위해 사용되는 함수입니다.
	/// </summary>
	/// <typeparam name="TRunableBehavior">생성시킬 행동 객체 형식을 전달합니다.</typeparam>
	/// <returns></returns>
	protected static System.Func<RunnableBehavior> Create<TRunableBehavior>()
		where TRunableBehavior : RunnableBehavior, new()
		=> () => new TRunableBehavior();
	//{ return () => new TRunableBehavior(); }
}
