﻿using System.Collections;
using UnityEngine;



public class SampleTask2 : RunnableBehavior
{
    public override IEnumerator RunBehavior()
    {
        Debug.Log("두 번쨰 행동 실행됨.");
        yield return new WaitForSeconds(1);


        //행동 실행 결과 성공
        isSucceeded = true;
    }
}

