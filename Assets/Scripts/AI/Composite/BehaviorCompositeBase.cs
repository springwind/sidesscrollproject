﻿using UnityEngine;
using System.Collections.Generic;


/// <summary>
/// 컴포짓을 나타내기 위한 클래스입니다.
/// </summary>
public abstract class BehaviorCompositeBase : RunnableBehavior
{
	/// <summary>
	/// 순차적으로 실행시킬 행동 객체들을 나타냅니다.
	/// </summary>
	protected List<System.Func<RunnableBehavior>> m_Runnables = new();

	public BehaviorCompositeBase() { }
	public BehaviorCompositeBase(params System.Func<RunnableBehavior>[] runnables)
	{
		foreach(System.Func<RunnableBehavior> runnable in runnables)
		{
			m_Runnables.Add(runnable);
		}
	}


	/// <summary>
	/// 행동을 추가합니다.
	/// 기본 생성자를 제공하는 행동 클래스만 사용 가능합니다.
	/// </summary>
	/// <typeparam name="TRunableBehavior">추가할 행동 형식을 전달합니다.</typeparam>
	public void AddBehavior<TRunableBehavior>()
		where TRunableBehavior : RunnableBehavior, new()
	{
		m_Runnables.Add(Create<TRunableBehavior>());
	}

	/// <summary>
	/// 행동을 추가합니다.
	/// 기본 생성자를 제공하지 않는 클래스의 경우 이 메서드를 사용하여 행동을 추가해야 합니다.
	/// </summary>
	/// <param name="runnable"></param>
	public void AddBehavior(System.Func<RunnableBehavior> runnable)
	{
		m_Runnables.Add(runnable);
	}



}
