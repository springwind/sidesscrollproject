using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
	[Header("# 최대 체력")]
	public float m_MaxHp;

	/// <summary>
	/// 현재 체력을 나타냅니다.
	/// </summary>
	private float _CurrentHp;

	/// <summary>
	/// 이 캐릭터를 조종하고 있는 플레이어 컨트롤러를 나타냅니다.
	/// </summary>
	private PlayerController _PlayerController;

	/// <summary>
	/// 이동 기능을 담당하는 컴포넌트입니다.
	/// </summary>
	private PlayerMovement _MovementComponent;

	/// <summary>
	/// Animator 컴포넌트를 제어하기 위한 컴포넌트입니다.
	/// </summary>
	private PlayerCharacterAnimController _AnimController;

	/// <summary>
	/// 공격 기능을 담당하는 컴포넌트입니다.
	/// </summary>
	private PlayerCharacterAttack _AttackComponent;

	/// <summary>
	/// 플레이어 UI 객체를 나타냅니다.
	/// </summary>
	private PlayerUI _PlayerUI;

	/// <summary>
	/// 플레이어 컨트롤러에게 조종당하고 있음을 나타냅니다.
	/// </summary>
	public bool isControlled => _PlayerController != null;

	private void Awake()
	{
		// 최대 체력으로 초기화
		_CurrentHp = m_MaxHp;

		GameSceneInstance sceneInstance = SceneManager.instance.GetSceneInstance<GameSceneInstance>();
		_PlayerUI = sceneInstance.m_UsePlayerUI;
		_PlayerUI.m_PlayerHpPanel.InitializeHpPanel(m_MaxHp);
		
        _MovementComponent = GetComponent<PlayerMovement>();
		_AnimController = GetComponentInChildren<PlayerCharacterAnimController>();
		_AttackComponent = GetComponent<PlayerCharacterAttack>();
	}

	private void Start()
	{
		// 애니메이션 이벤트 바인딩
		BindAnimationEvents();

		// 공격 이벤트 바인딩
		BindAttackEvents();
	}

	private void Update()
	{
		UpdateAnimControllerParam();
	}

	/// <summary>
	/// 애니메이션 이벤트를 바인딩합니다.
	/// </summary>
	private void BindAnimationEvents()
	{
		_AnimController.onNextAttackCheckStarted += _AttackComponent.OnNextAttackCheckStarted;
		_AnimController.onNextAttackCheckFinished += _AttackComponent.OnNextAttackCheckFinished;

		_AnimController.onAttackStarted += _AttackComponent.OnAttackStarted;
		_AnimController.onAttackFinished += _AttackComponent.OnAttackFinished;

		_AnimController.onAttackAreaEnabled += _AttackComponent.EnableAttackArea;
		_AnimController.onAttackAreaDisabled += _AttackComponent.DisableAttackArea;
	}

	/// <summary>
	/// 공격 이벤트를 바인딩합니다.
	/// </summary>
	private void BindAttackEvents()
	{
		_AttackComponent.onAttackStarted += () => _MovementComponent.AllowMovementInput(false);
		_AttackComponent.onAttackFinished += () => _MovementComponent.AllowMovementInput(true);

		_MovementComponent.onDirectionChanged += _AttackComponent.UpdateDirection;
	}

	/// <summary>
	/// 애님 컨트롤러 파라미터 값을 갱신합니다.
	/// </summary>
	private void UpdateAnimControllerParam()
	{
		_AnimController.isMove = _MovementComponent.velocity.sqrMagnitude > 0.0f;
		_AnimController.isRight = _MovementComponent.isRight;
		_AnimController.isGrouneded = _MovementComponent.isGrounded;
		_AnimController.comboCount = _AttackComponent.comboCount;

	}

	/// <summary>
	/// 이 캐릭터의 조종이 시작될 때 호출됩니다.
	/// </summary>
	/// <param name="playerController">조종을 시작하는 플레이어 컨트롤러 객체가 전달됩니다.</param>
	public virtual void OnControlStarted(PlayerController playerController)
	{
		_PlayerController = playerController;
	}

	/// <summary>
	/// 이 캐릭터의 조종이 끝났을 때 호출됩니다.
	/// </summary>
	public virtual void OnControlFinished()
	{
		_PlayerController = null;
	}

	public void OnMovementInput(Vector2 axisValue) => _MovementComponent.OnMovementInput(axisValue);
	public void OnJumpInput() => _MovementComponent.OnJumpInput();
	public void OnAttackInput() => _AttackComponent.OnAttackKeyPressed();

	public void OnDamaged(float damage)
	{
		// 체력 감소
		_CurrentHp -= damage;

		// UI 에 표시합니다.
		_PlayerUI.m_PlayerHpPanel.SetHpValue(_CurrentHp);
	}

}

